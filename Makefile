encrypt: 
	@test -e secrets.yaml || ( echo "Error: nothing to encrypt"; exit 1 )
	@gpg --recipient E6D6331D --encrypt-files secrets.yaml && rm secrets.yaml

decrypt:
	@test -e secrets.yaml || gpg --decrypt secrets.yaml.gpg > secrets.yaml

view: decrypt
	@view secrets.yaml

edit: decrypt
	@editor secrets.yaml

sync:
	@git commit -am "update secrets" || true
	@echo github
	@git push git@github.com:resmo/secrets.git master
	@echo bitbucket
	@git push git@bitbucket.org:resmo/secrets.git master
